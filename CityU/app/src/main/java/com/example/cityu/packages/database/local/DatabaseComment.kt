package com.example.cityu.packages.database.Comment

import androidx.room.*
import com.example.cityu.packages.database.remote.DevByteComment

@Entity
data class DatabaseComment(
        @PrimaryKey val id: String = "",
        @ColumnInfo(name = "user_uid") val userUid: String,
        @ColumnInfo(name = "user_name") val name: String,
        @ColumnInfo(name = "date") val date: String,
        @ColumnInfo(name = "user_comment") val comment: String,
        @ColumnInfo(name = "num_stars") val numStars: Float,
        @ColumnInfo(name = "localId") val localId: String? = "",
        @ColumnInfo(name = "productId") val productId: String?)

/**
 * Map DatabaseVideos to domain entities
 */
fun List<DatabaseComment>.asDomainModel(): List<DevByteComment> {
    return map {
        DevByteComment(
                id = it.id,
                userUid = it.userUid,
                name = it.name,
                date = it.date,
                comment  = it.comment,
                num_stars = it.numStars,
                localId = it.localId,
                productId = it.productId)
    }
}