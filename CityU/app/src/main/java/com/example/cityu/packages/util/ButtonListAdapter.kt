package com.example.cityu.packages.util

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.ActivityLogin
import com.example.cityu.packages.activities.ActivityMainMenu
import com.example.cityu.packages.database.remote.DevByteOptions
import com.google.firebase.auth.FirebaseAuth

class   ButtonListAdapter internal constructor(
        context: Context
) : RecyclerView.Adapter<ButtonListAdapter.OptionViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var options = emptyList<DevByteOptions>() // Cached copy of options

    private lateinit var mauth: FirebaseAuth

    inner class OptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView = itemView.findViewById(R.id.button_image)
        var text: TextView = itemView.findViewById(R.id.button_name)
        var type: String = "recom"
        val item: View? = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {

        var itemView = inflater.inflate(R.layout.button_card_view, parent, false)
        val aux = OptionViewHolder(itemView)
        mauth = FirebaseAuth.getInstance()
        return aux
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        val current = options[position]
        holder.text.text = current.name
        holder.type = current.type

        val cm = holder.itemView.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        when(current.type){
            "log_out" -> {
                holder.image.setImageResource(R.drawable.ic_exit_to_app_black_24dp)
                holder.item?.setOnClickListener {
                    mauth.signOut()
                    val intent = Intent(it.context, ActivityLogin::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(it.context,intent,null)
                }
            }
            "profile" -> {
                if (!isConnected) {
                    holder.image.setBackgroundResource(R.color.colorPrimaryDark)
                }
                holder.item?.setOnClickListener {
                    val cm = holder.itemView.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

                    if (isConnected)
                        it.findNavController().navigate(R.id.action_options_to_fragmentProfile)
                    else
                        Toast.makeText(holder.item.context, "Sin Conexión. Conectate a la red y vuelve a intentarlo", Toast.LENGTH_SHORT).show()
                }
            }
            "recom" -> {
                if (!isConnected) {
                    holder.image.setBackgroundResource(R.color.colorPrimaryDark)
                }
                holder.image.setImageResource(R.drawable.ic_info_outline_black_24dp)
                holder.item?.setOnClickListener {
                    val cm = holder.itemView.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

                    if (isConnected)
                        it.findNavController().navigate(R.id.action_options_to_fragmentRecomendations)
                    else
                        Toast.makeText(holder.item.context, "Sin Conexión. Conectate a la red y vuelve a intentarlo", Toast.LENGTH_SHORT).show()
                }
            }
            "menu" -> {
                if (!isConnected) {
                    holder.image.setBackgroundResource(R.color.colorPrimaryDark)
                }
                holder.image.setImageResource(R.drawable.ic_photo_filter_black_24dp)
            }
            else -> {
                when(current.type){
                    "bares" -> holder.image.setImageResource(R.drawable.bares)
                    "cafes" -> holder.image.setImageResource(R.drawable.cafeterias)
                    "panaderias" -> holder.image.setImageResource(R.drawable.panaderias)
                    "postres" -> holder.image.setImageResource(R.drawable.postres)
                    "restaurantes" -> holder.image.setImageResource(R.drawable.restaurantes)
                }
                holder.item?.setOnClickListener {it.findNavController().navigate(R.id.action_fragmentMainCategories_to_main_locals, bundleOf("category" to current.type, "title" to current.name))}
            }
        }
    }

    internal fun setOptions(options: List<DevByteOptions>) {
        this.options = options
        notifyDataSetChanged()
    }

    override fun getItemCount() = options.size
}