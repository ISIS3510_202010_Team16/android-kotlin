package com.example.cityu.packages.database.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ProductoDao {
    /*-------------------------------------GET----------------------------------------------*/
    @Query("SELECT * from DatabaseProducto WHERE type_value != 'Special'")
    fun getProducts(): LiveData<List<DatabaseProducto>>

    @Query("SELECT * from DatabaseProducto WHERE type = 'Promotion' AND type_value != 'Special'")
    fun getPromotions(): LiveData<List<DatabaseProducto>>

    @Query("SELECT * from DatabaseProducto WHERE localId=:localId AND type_value != 'Special'")
    fun getProductsByLocal(localId: String): LiveData<List<DatabaseProducto>>

    @Query("SELECT * from DatabaseProducto WHERE id=:productId")
    suspend fun getProductsById(productId: Int): List<DatabaseProducto>

    /*-------------------------------------INSERT----------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(product: DatabaseProducto)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(products: List<DatabaseProducto>)

    /*-------------------------------------DELETE----------------------------------------------*/
    @Query("DELETE FROM DatabaseProducto")
    suspend fun deleteAll()
}