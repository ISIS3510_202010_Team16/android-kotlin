package com.example.cityu.packages.database.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.cityu.packages.database.Comment.DatabaseComment

@Dao
interface CommentDao {
    /*-------------------------------------GET----------------------------------------------*/
    @Query("SELECT * from DatabaseComment WHERE localId=:localId")
    fun getCommentsByLocalId(localId: String): LiveData<List<DatabaseComment>>

    @Query("SELECT * from DatabaseComment WHERE productId=:productId")
    fun getCommentsByProductId(productId: String): LiveData<List<DatabaseComment>>

    @Query("SELECT * from DatabaseComment")
    fun getComments(): LiveData<List<DatabaseComment>>


    /*-------------------------------------INSERT----------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(comment: DatabaseComment)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll( comments: List<DatabaseComment>)

    /*-------------------------------------UPDATE----------------------------------------------*/


    /*-------------------------------------DELETE----------------------------------------------*/
    @Query("DELETE FROM DatabaseComment")
    suspend fun deleteAll()
}