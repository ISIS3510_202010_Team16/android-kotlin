package com.example.cityu.packages.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.database.local.asDomainModel
import com.example.cityu.packages.database.remote.DevByteLocal
import com.example.cityu.packages.services.LocalViewModel
import com.example.cityu.packages.util.LocalListAdapter
import kotlinx.coroutines.runBlocking

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [main_locals.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [main_locals.newInstance] factory method to
 * create an instance of this fragment.
 */
class main_locals : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var localViewModel: LocalViewModel

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_main_locals, container, false)
        data = bundleOf("title" to arguments?.getString("title"), "frag" to "locals")
        passData(data)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        val lmGrid = GridLayoutManager(view.context, 2)
        recyclerView.layoutManager = lmGrid
        val adapter = LocalListAdapter(view.context, "mainLocals")
        recyclerView.adapter = adapter

        localViewModel = ViewModelProvider(this).get(LocalViewModel::class.java)
        runBlocking {
            var locals: List<DevByteLocal> = listOf()
            when(arguments?.getString("category")){
                "bares" -> locals = localViewModel.getLocalsByCategoryId(Constantes.BARES).asDomainModel()
                "cafes" -> locals = localViewModel.getLocalsByCategoryId(Constantes.CAFES).asDomainModel()
                "panaderias" -> locals = localViewModel.getLocalsByCategoryId(Constantes.PANADERIAS).asDomainModel()
                "postres" -> locals = localViewModel.getLocalsByCategoryId(Constantes.POSTRES).asDomainModel()
                "restaurantes" -> locals = localViewModel.getLocalsByCategoryId(Constantes.RESTAURANTES).asDomainModel()
            }
            adapter.setLocals(locals)
        }
        // Inflate the layout for this fragment
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }

    object Constantes{
        const val BARES = 1
        const val CAFES = 2
        const val PANADERIAS = 3
        const val POSTRES = 4
        const val RESTAURANTES = 5

    }
}
