package com.example.cityu.packages.services

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.cityu.packages.database.local.*
import com.example.cityu.packages.database.remote.DevByteLocal
import com.example.cityu.packages.database.remote.DevBytePedido
import com.example.cityu.packages.database.remote.NetworkPedido
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException

class PedidosViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: PedidosRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only updatePedidoAndProductosPedido the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allPedidos: LiveData<List<DevBytePedido>>

    init {
        val pedidosDao = AppRoomDatabase.getDatabase(application, viewModelScope).pedidosDao()
        repository = PedidosRepository(pedidosDao)
        refreshDataFromRepository(FirebaseAuth.getInstance().currentUser?.uid!!)
        allPedidos = repository.allPedidos
    }

    suspend fun getlocalById(localId: String): List<DevByteLocal>{
        return repository.getLocalById(localId)
    }

    suspend fun getPedidoById(pedidoId: String): List<DevBytePedido>{
        return repository.getPedidoById(pedidoId)
    }

    suspend fun getPedidoIncompleteWithProductoPedido(): List<PedidoWithProductoPedido>{
        return repository.getPedidoIncompleteWithProductoPedido()
    }

    fun getPedidosWithProductoPedidoAndProducto(): LiveData<List<PedidoWithProductoPedido>>{
        return repository.getPedidosWithProductoPedidoAndProducto()
    }
    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insertPedido(pedido: Pedido) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertPedido(pedido)
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insertProductoPedido(productoPedido: ProductoPedido) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertProductoPedido(productoPedido)
    }

    fun insertPedidoAndProductoPedido(pedido: Pedido, productoPedido: ProductoPedido) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertPedidoAndProductoPedido(pedido, productoPedido)
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    suspend fun getPedidoIncomplete(): Pedido{
        return repository.getPedidoIncomplete()
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun updatePedidoAndProductosPedido(pedido: Pedido, productosPedido: ProductoPedido) = viewModelScope.launch(Dispatchers.IO) {
        repository.updatePedidoAndProductosPedido(pedido, productosPedido)
    }

    suspend fun deletePedidoIncomplete(){
        repository.deletePedidoIncomplete()
    }

    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    private fun refreshDataFromRepository(uid: String) {
        val database = FirebaseDatabase.getInstance().reference
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to updatePedidoAndProductosPedido the UI
                val pedidos = dataSnapshot.children.mapNotNull { it.getValue<NetworkPedido>(NetworkPedido::class.java) }
                viewModelScope.launch {
                    try {
                        repository.refreshPedidos(pedidos)
                        _eventNetworkError.value = false
                        _isNetworkErrorShown.value = false
                    } catch (networkError: IOException) {
                        // Show a Toast error message and hide the progress bar.
                        if (allPedidos.value.isNullOrEmpty())
                            _eventNetworkError.value = true
                    }

                    // ...
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        database.child("Pedidos").child(uid).addValueEventListener(postListener)
    }

    /**
     * Event triggered for network error. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _eventNetworkError = MutableLiveData<Boolean>(false)


    /**
     * Flag to display the error message. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _isNetworkErrorShown = MutableLiveData<Boolean>(false)
}