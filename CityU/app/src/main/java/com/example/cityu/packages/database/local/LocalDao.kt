package com.example.cityu.packages.database.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.cityu.packages.database.Comment.DatabaseComment

@Dao
interface LocalDao {
    /*-------------------------------------GET-------------------------------------------------*/
    @Query("SELECT * from DatabaseLocal")
    fun getLocals(): LiveData<List<DatabaseLocal>>

    @Query("SELECT * from DatabaseLocal WHERE id = :localId")
    suspend fun getLocalByLocalId(localId: String): List<DatabaseLocal>

    @Query("SELECT * from DatabaseComment WHERE localId=:localId")
    suspend fun getCommentsByLocalId(localId: String): List<DatabaseComment>

    @Query("SELECT * from DatabaseLocal WHERE category=:categoryId")
    suspend fun getLocalsByCategoryId(categoryId: Int): List<DatabaseLocal>

    /*-------------------------------------INSERT----------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(local: DatabaseLocal)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll( locales: List<DatabaseLocal>)

    /*-------------------------------------UPDATE----------------------------------------------*/
    @Transaction
    suspend fun updateNumStars(localId: String, stars: Float){
        val local = getLocalByLocalId(localId)
        val comm = getCommentsByLocalId(localId)
        if(local.isNotEmpty() && comm.isNotEmpty()) {
            local[0].num_stars = (stars + local[0].num_stars)/comm.size
            updateLocal(local[0])
        }
    }

    @Update
    suspend fun updateLocal(vararg local: DatabaseLocal)

    /*-------------------------------------DELETE----------------------------------------------*/
    @Query("DELETE FROM DatabaseLocal")
    suspend fun deleteAll()
}