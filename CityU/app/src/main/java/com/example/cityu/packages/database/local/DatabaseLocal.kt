package com.example.cityu.packages.database.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.cityu.packages.database.remote.DevByteLocal

@Entity
data class DatabaseLocal(
        @PrimaryKey val id: String = "",
        @ColumnInfo(name = "name") val name: String?,
        @ColumnInfo(name = "addr") val addr: String?,
        @ColumnInfo(name = "phone") val phone: String?,
        @ColumnInfo(name = "web_page") val web_page: String?,
        @ColumnInfo(name = "wait_time") val wait_time: Int?,
        @ColumnInfo(name = "num_stars") var num_stars: Float = 0.toFloat(),
        @ColumnInfo(name = "category") var category: Int = 1,
        @ColumnInfo(name = "num_comments") var num_comments: Int = 0,
        @ColumnInfo(name = "url_image") var url_image: String = "")

/**
 * Map DatabaseVideos to domain entities
 */
fun List<DatabaseLocal>.asDomainModel(): List<DevByteLocal> {
    return map {
        DevByteLocal(
                id = it.id,
                name = it.name,
                addr = it.addr,
                phone = it.phone,
                web_page = it.web_page,
                wait_time = it.wait_time,
                num_stars = it.num_stars,
                category = it.category,
                num_comments = it.num_comments,
                url_image = it.url_image)
    }
}