package com.example.cityu.packages.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.services.PedidosViewModel
import com.example.cityu.packages.util.PedidoListAdapter
import kotlinx.android.synthetic.main.fragment_main_pedidos.view.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentPedidos : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var pedidosViewModel: PedidosViewModel

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_main_pedidos, container, false)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        recyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        val adapter = PedidoListAdapter(view.context)
        recyclerView.adapter = adapter

        data = bundleOf("title" to "Pedidos", "frag" to "pedidos")
        passData(data)

        pedidosViewModel = ViewModelProvider(this).get(PedidosViewModel::class.java)
        pedidosViewModel.allPedidos.observe(viewLifecycleOwner, Observer { pedidos ->
            // Update the cached copy of the words in the adapter.
            pedidos?.let { adapter.setPedidos(it) }
            if(pedidos.isEmpty()) {
                view.reload.visibility = View.VISIBLE
                Toast.makeText(activity, "No tienes pedidos aún", Toast.LENGTH_LONG).show()
            }
            else
                view.reload.visibility = View.GONE
        })

        view.reload.setOnClickListener{
            val cm = view.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

            if (isConnected) {
                if (pedidosViewModel.allPedidos.value.isNullOrEmpty()) {
                    Toast.makeText(activity, "No tienes pedidos aún", Toast.LENGTH_LONG).show()
                } else {
                    view.reload.visibility = View.GONE
                }
            }
            else
                Toast.makeText(activity, "Sin Conexión. Conectate a la red y vuelve a intentarlo", Toast.LENGTH_LONG).show()
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }
}

