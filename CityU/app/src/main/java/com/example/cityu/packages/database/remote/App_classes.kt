package com.example.cityu.packages.database.remote

data class DevByteLocal(
        var id: String = "",
        var name: String? = "",
        var addr: String? = "",
        var phone: String? = "",
        var web_page: String? = "",
        var wait_time: Int? = 30,
        var num_stars: Float = 0.toFloat(),
        var category: Int = 1,
        var num_comments: Int = 0,
        var url_image: String = "")

data class DevByteProducto(
        var id: String = "",
        var name: String = "",
        var value: Int = 0,
        var type: String = "",
        var type_value: String = "",
        var localId: String = "",
        var url_product: String = "")


data class DevBytePedido(
        var id: String = "",
        var productosPedidos: List<DevByteProductoPedido>? = arrayListOf(),
        var total_value: Int? = 0,
        var complete: Int? = 0,
        var date: String = "")


data class DevByteProductoPedido(
        var pedidoId: String = "",
        var productId: String = "",
        var productName: String = "",
        var quantity: Int? = 0,
        var cost: Int? = 0,
        var localId: String = "")

data class DevByteComment(
        var id: String = "",
        var userUid: String = "",
        var name: String = "",
        var date: String = "",
        var comment: String = "",
        var num_stars: Float = 0.toFloat(),
        var localId: String? = "",
        var productId: String? = "")

data class DevByteOptions(
        var name: String = "",
        var type: String = ""
)

