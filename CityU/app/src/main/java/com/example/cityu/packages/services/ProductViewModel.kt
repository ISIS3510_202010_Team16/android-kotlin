package com.example.cityu.packages.services

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.cityu.packages.database.local.AppRoomDatabase
import com.example.cityu.packages.database.local.DatabaseProducto
import com.example.cityu.packages.database.local.ProductoRepository
import com.example.cityu.packages.database.remote.DevByteProducto
import com.example.cityu.packages.database.remote.NetworkProducto
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException

class ProductViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ProductoRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only updatePedidoAndProductosPedido the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allProducts: LiveData<List<DevByteProducto>>
    val allPromotions: LiveData<List<DevByteProducto>>

    init {
        val productsDao = AppRoomDatabase.getDatabase(application, viewModelScope).productDao()
        repository = ProductoRepository(productsDao)
        refreshDataFromRepository()
        allProducts = repository.allProducto
        allPromotions = repository.allPromotions
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(product: DatabaseProducto) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(product)
    }

    fun getProductsByLocal(localId: String): LiveData<List<DevByteProducto>>{
        return repository.getProductsByLocal(localId)
    }

    suspend fun getProductById(productId: Int): List<DatabaseProducto>{
        return repository.getProductById(productId)
    }


    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    private fun refreshDataFromRepository() {
        val database = FirebaseDatabase.getInstance().reference
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to updatePedidoAndProductosPedido the UI
                val products = dataSnapshot.children.mapNotNull { it.getValue<NetworkProducto>(NetworkProducto::class.java) }
                viewModelScope.launch {
                    try {
                        repository.refreshProducto(products)
                        _eventNetworkError.value = false
                        _isNetworkErrorShown.value = false
                    } catch (networkError: IOException) {
                        // Show a Toast error message and hide the progress bar.
                        if (allProducts.value.isNullOrEmpty())
                            _eventNetworkError.value = true
                    }

                    // ...
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        database.child("Products").addValueEventListener(postListener)
    }

    /**
     * Event triggered for network error. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _eventNetworkError = MutableLiveData<Boolean>(false)


    /**
     * Flag to display the error message. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _isNetworkErrorShown = MutableLiveData<Boolean>(false)

}