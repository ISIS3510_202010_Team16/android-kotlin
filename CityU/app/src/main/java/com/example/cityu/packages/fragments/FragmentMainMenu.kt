package com.example.cityu.packages.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.services.LocalViewModel
import com.example.cityu.packages.services.ProductViewModel
import com.example.cityu.packages.util.LocalListAdapter
import com.example.cityu.packages.util.ProductListAdapter
import kotlinx.android.synthetic.main.fragment_main_menu.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Fragment_main_menu.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Fragment_main_menu.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentMainMenu : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var localViewModel: LocalViewModel
    private lateinit var productViewModel: ProductViewModel

    private var doubleBackToExitPressedOnce = false

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_main_menu, container, false)
        view.todosTxt.setOnClickListener {
            view.findNavController().navigate(R.id.action_fragment_main_menu_to_main_categories)
        }
        view.all_promotions.setOnClickListener {
            view.findNavController().navigate(R.id.action_fragment_main_menu_to_main_promotions)
        }
        view.pedidos_id.setOnClickListener {
            view.findNavController().navigate(R.id.action_fragment_main_menu_to_fragmentPedidos)
        }

        data = bundleOf("title" to "City U Market", "frag" to "main")
        passData(data)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        recyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.HORIZONTAL, false)
        val adapter = LocalListAdapter(view.context,"mainMenu")
        recyclerView.adapter = adapter

        localViewModel = ViewModelProvider(this).get(LocalViewModel::class.java)
        localViewModel.allLocals.observe(this, Observer { locals ->
            // Update the cached copy of the words in the adapter.
            locals?.let { adapter.setLocals(it) }
        })

        val promotionsRecyclerView: RecyclerView = view.findViewById(R.id.recycler_promotionsId)
        promotionsRecyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        val adapter2 = ProductListAdapter(view.context,"main")
        promotionsRecyclerView.adapter = adapter2

        productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)
        productViewModel.allPromotions.observe(viewLifecycleOwner, Observer { promotions ->
            // Update the cached copy of the words in the adapter.
            promotions?.let { adapter2.setPromotions(it) }
        })
        // Inflate the layout for this fragment
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }
}
