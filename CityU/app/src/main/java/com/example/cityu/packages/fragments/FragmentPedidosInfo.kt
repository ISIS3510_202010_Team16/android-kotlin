package com.example.cityu.packages.fragments


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.database.local.convertToDevBytePedido
import com.example.cityu.packages.database.remote.DevByteProductoPedido
import com.example.cityu.packages.services.PedidosViewModel
import com.example.cityu.packages.util.ProductoPedidoListAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_pedido.view.*
import kotlinx.coroutines.runBlocking

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentPedidosInfo.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentPedidosInfo : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var pedidosViewModel: PedidosViewModel

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_pedido, container, false)
        pedidosViewModel = ViewModelProvider(this).get(PedidosViewModel::class.java)

        data = bundleOf("title" to "Información Pedido", "frag" to "pedidoInfo")
        passData(data)

        runBlocking {
            var aux = pedidosViewModel.getlocalById(arguments?.getString("localId")!!)
            if(aux.isNotEmpty()) {
                view.findViewById<TextView>(R.id.local_name).text = aux[0].name
                view.findViewById<TextView>(R.id.local_wait_time).text = aux[0].wait_time.toString()
            }
        }
        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        recyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        val adapter = ProductoPedidoListAdapter(view.context,"pedidos")
        recyclerView.adapter = adapter

        runBlocking {
            val pedido = pedidosViewModel.getPedidoById(arguments?.getString("id")!!)
            adapter.setProductoPedido(pedido[0].productosPedidos!!)
            view.valor_pedido.text = "$${pedido[0].total_value.toString()}"
        }


        return view
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentPedidosInfo.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentPedidosInfo().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }
}
