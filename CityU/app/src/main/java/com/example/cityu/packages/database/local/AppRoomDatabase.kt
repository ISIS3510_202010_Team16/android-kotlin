package com.example.cityu.packages.database.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.cityu.packages.database.Comment.DatabaseComment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = arrayOf(DatabaseProducto::class, DatabaseLocal::class, DatabaseComment::class, Pedido::class, ProductoPedido::class), version = 1, exportSchema = false)
abstract class AppRoomDatabase : RoomDatabase() {

    abstract fun localDao(): LocalDao
    abstract fun commentDao(): CommentDao
    abstract fun productDao(): ProductoDao
    abstract fun pedidosDao(): PedidosDao

    companion object {
        @Volatile
        private var INSTANCE: AppRoomDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): AppRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppRoomDatabase::class.java,
                        "App_database"
                ).addCallback(LocalDatabaseCallback(scope)).build()
                INSTANCE = instance
                return instance
            }
        }
    }

    private class LocalDatabaseCallback(
            private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.localDao(), database.commentDao(), database.productDao(), database.pedidosDao())
                }
            }
        }

        fun populateDatabase(localDao: LocalDao, commentDao: CommentDao, promotionDao: ProductoDao, pedidosDao: PedidosDao) {
            // Delete all content here.
            /*localDao.deletePedidoIncomplete()
            commentDao.deleteAll()
            productDao.deletePedidoIncomplete()
            pedidosDao.deletePedidoIncomplete()
            pedidosDao.deletePedidos()
            pedidosDao.deleteAllProductos()*/


            //val sdf = SimpleDateFormat("dd/M/yyyy")
            //val currentDate = sdf.format(Date())
            // Add sample words.
            /*var local = DatabaseLocal(1, "Alitas Colombianas", "Cra 100# 94-34", "3102020578", "www.alitascolombianas.com", 5)
            localDao.insert(local)
            local = DatabaseLocal(2, "Hornitos", "Cra 100# 94-34", "3102020578", "www.hornitos.com", 25)
            localDao.insert(local)
            local = DatabaseLocal(3, "Pizza Hut", "Cra 100# 94-34", "3102020578", "www.pizzahut.com", 13)
            localDao.insert(local)

            val sdf = SimpleDateFormat("dd/M/yyyy")
            val currentDate = sdf.format(Date())
            // Add sample words.
            var comment = DatabaseComment(1, "Mateo Salcedo Rodriguez", currentDate, "Muy buen negocio para compartir en familia",1)
            commentDao.insert(comment)
            comment = DatabaseComment(2, "Santiago Robayo", currentDate, "Deliciosos platos acompañados de un gran ambiente",1)
            commentDao.insert(comment)

            // Add sample words.
            var promotion = DatabaseProducto(1, "Expresso de caramelo",3000, "2x1",1)
            productDao.insert(promotion)
            promotion = DatabaseProducto(2, "Combo desayuno americano",3000, "50% off",2)
            productDao.insert(promotion)*/

            // TODO: Add your own words!
        }
    }
}
