package com.example.cityu.packages.fragments


import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.ActivityLogin
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.database.remote.DevByteOptions
import com.example.cityu.packages.util.ButtonListAdapter
import com.google.firebase.auth.FirebaseAuth

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [options.newInstance] factory method to
 * create an instance of this fragment.
 */
class options : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_options, container, false)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        val lmGrid = GridLayoutManager(view.context, 2)
        recyclerView.layoutManager = lmGrid
        val adapter = ButtonListAdapter(view.context)
        recyclerView.adapter = adapter

        val idOptions = listOf("recom", "menu", "log_out", "profile")
        val options = listOf(DevByteOptions("Recomendaciones", idOptions[0]), DevByteOptions("Analizar Menu", idOptions[1]),
                DevByteOptions("Cerrar Sesión", idOptions[2]), DevByteOptions( "Perfil", idOptions[3]))
        adapter.setOptions(options)

        data = bundleOf("title" to "Opciones", "frag" to "opc")
        passData(data)

        return view
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment options.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                options().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
