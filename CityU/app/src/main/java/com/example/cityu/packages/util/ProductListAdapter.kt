package com.example.cityu.packages.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKeys
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.cityu.R
import com.example.cityu.packages.database.remote.DevByteProducto
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.product_cardview.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File

class ProductListAdapter internal constructor(
        context: Context,
        ye: String
) : RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var products = emptyList<DevByteProducto>() // Cached copy of products
    private var ye = ye

    private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
    private val masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var id: String = ""
        var name: String = ""
        var value: Int = 0
        var type: String = ""
        var localId: String = ""
        val item: View? = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {

        var itemView = inflater.inflate(R.layout.product_cardview, parent, false)
        val aux = ProductViewHolder(itemView)
        itemView.pedido.setOnClickListener{
            var bundle = bundleOf("id" to aux.id, "name" to aux.name, "value" to aux.value, "type" to aux.type, "localId" to aux.localId, "seePedido" to false)
            if(ye == "main")
                it.findNavController().navigate(R.id.action_fragment_main_menu_to_pago, bundle)
            else if (ye == "promo")
                it.findNavController().navigate(R.id.action_main_promotions_to_pago,bundle)
            else if (ye == "recom")
                it.findNavController().navigate(R.id.action_fragmentRecomendations_to_pago,bundle)
            else
                it.findNavController().navigate(R.id.action_main_local_to_pago, bundle)
        }
        itemView.setOnClickListener{
            var bundle = bundleOf("id" to aux.id, "name" to aux.name, "value" to aux.value, "type" to aux.type, "localId" to aux.localId, "seePedido" to false, "url_image" to aux.id)
            when(ye){
                "main" -> it.findNavController().navigate(R.id.action_fragment_main_menu_to_blankFragment, bundle)
                "local" -> it.findNavController().navigate(R.id.action_main_local_to_blankFragment, bundle)
                "promo" -> it.findNavController().navigate(R.id.action_main_promotions_to_blankFragment, bundle)
                "recom" -> it.findNavController().navigate(R.id.action_fragmentRecomendations_to_blankFragment, bundle)
            }
        }
        return aux
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val current = products[position]
        holder.item?.findViewById<TextView>(R.id.promotion_name)?.text = current.name
        holder.item?.findViewById<TextView>(R.id.promotion_value)?.text = current.value.toString()

        val type = holder.item?.findViewById<TextView>(R.id.promotion_type)
        type?.text = "${current.type} - ${current.type_value}"
        holder.type = "${current.type} - ${current.type_value}"
        if(current.type_value.isBlank()){
            type?.setTextColor(ContextCompat.getColor(holder.item.context, R.color.colorAccent))
            type?.text = "${current.type}"
            holder.type = "${current.type}"
        }
        if(current.type_value.isNotBlank())
            when(current.type){
                "Promotion" -> {
                    if(current.type_value=="Special")
                        type?.setTextColor(ContextCompat.getColor(holder.item.context, R.color.colorSpecial))
                    else
                        type?.setTextColor(ContextCompat.getColor(holder.item.context, R.color.colorBoard))
                }
            }

        holder.id = current.id
        holder.name = current.name
        holder.value = current.value
        holder.localId = current.localId

        if(current.url_product.isNotBlank()) {
            val storage = FirebaseStorage.getInstance()
            val gsReference = storage.getReferenceFromUrl(current.url_product)

            var file = File("/data/data/com.example.cityu/images/", "JPEG_${current.id}.jpg")
            if (!file.exists()) {
                val cm = holder.itemView.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

                if (isConnected) {
                    markButtonEnable(holder.itemView.pedido, holder.itemView)
                    Glide.with(holder.itemView.context)
                            .asBitmap()
                            .load(gsReference)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                    saveImage(resource, holder.item!!, current.id)
                                    loadImages(holder.itemView, current.id)
                                }
                            })
                }
                else{
                    markButtonDisable(holder.itemView.pedido, holder.itemView)
                    var localPhoto: ImageView = holder.itemView.findViewById(R.id.product_photo)
                    localPhoto.setImageResource(R.drawable.ic_broken_image_black_24dp)
                    localPhoto.setBackgroundResource(R.color.colorPrimaryDark)
                }
            } else {
                val cm = holder.itemView.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

                if (!isConnected)
                    markButtonDisable(holder.itemView.pedido, holder.itemView)
                else
                    markButtonEnable(holder.itemView.pedido, holder.itemView)
                loadImages(holder.itemView, current.id)
            }
        }
    }

    internal fun setPromotions(promotions: List<DevByteProducto>) {
        this.products = promotions
        notifyDataSetChanged()
    }

    private fun markButtonDisable(button: Button, view: View) {
        button?.isEnabled = false
        button?.setBackgroundColor(ContextCompat.getColor(view.context, R.color.colorGray))
    }

    private fun markButtonEnable(button: Button, view: View) {
        button?.isEnabled = true
        button?.setBackgroundResource(R.drawable.buttonshape)
    }

    override fun getItemCount() = products.size

    private fun saveImage(image: Bitmap, itemView: View, productId: String){

        val imageFileName = "JPEG_${productId}.jpg"

        // Creates a file with this name, or replaces an existing file
        // that has the same name. Note that the file name cannot contain
        // path separators.
        val encryptedFile = EncryptedFile.Builder(
                File("/data/data/com.example.cityu/images/", imageFileName),
                itemView.context,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        try {
            val asd = encryptedFile.openFileOutput()
            image.compress(Bitmap.CompressFormat.JPEG, 100, asd)
            asd.close()
        } catch (error: Exception) {
            error.printStackTrace()
        }


    }

    private fun loadImages(itemView: View, productId: String){

        val imageFileName = "JPEG_${productId}.jpg"
        val encryptedFile2 = EncryptedFile.Builder(
                File("/data/data/com.example.cityu/images/", imageFileName),
                itemView.context,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        val contents = encryptedFile2.openFileInput()
        var img: Bitmap = BitmapFactory.decodeStream(contents)

        itemView.findViewById<ImageView>(R.id.product_photo).setImageBitmap(img)
    }
}