package com.example.cityu.packages.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKeys
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.cityu.R
import com.example.cityu.packages.database.remote.DevByteLocal
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.product_cardview.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File

class LocalListAdapter internal constructor(
        context: Context,
        str: String
) : RecyclerView.Adapter<LocalListAdapter.LocalViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var locals = emptyList<DevByteLocal>() // Cached copy of locals
    private val view = str

    private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
    private val masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

    inner class LocalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var waitTime: Int? = 0
        var name: String? = ""
        var localId: String = ""
        var numStars: Float = 0.toFloat()
        val item: View? = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocalViewHolder {

        var itemView = inflater.inflate(R.layout.main_locals_cardview, parent, false)
        if (view=="mainMenu") {
            itemView = inflater.inflate(R.layout.main_menu_local_cardview, parent, false)
        }

        val aux = LocalViewHolder(itemView)
        itemView.setOnClickListener {
            var bundle = bundleOf("name" to aux.name, "waitTime" to aux.waitTime, "localId" to aux.localId, "url_image" to aux.localId)
            itemView.findNavController().navigate(R.id.action_main_locals_to_local_main, bundle)
        }
        return aux
    }

    override fun onBindViewHolder(holder: LocalViewHolder, position: Int) {
        val current = locals[position]
        if(view!="mainMenu") {
            holder.item?.findViewById<TextView>(R.id.local_wait_time)?.text = current.wait_time.toString()
            holder.item?.findViewById<RatingBar>(R.id.ratingBar)?.rating = current.num_stars.toFloat()
        }
        holder.item?.findViewById<TextView>(R.id.local_name)?.text = current.name
        holder.name = current.name
        holder.waitTime = current.wait_time
        holder.localId = current.id

        if(current.url_image.isNotBlank()) {
            val storage = FirebaseStorage.getInstance()
            val gsReference = storage.getReferenceFromUrl(current.url_image)

            var file = File("/data/data/com.example.cityu/images/", "JPEG_${current.id}.jpg")
            if (!file.exists()) {                    val cm = holder.itemView.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

                if (isConnected) {
                    Glide.with(holder.itemView.context)
                            .asBitmap()
                            .load(gsReference)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                    saveImage(resource, holder.item!!, current.id)
                                    loadImages(holder.itemView, current.id)
                                }
                            })
                }
                else{
                    var localPhoto: ImageView = holder.itemView.findViewById(R.id.local_photo)
                    localPhoto.setImageResource(R.drawable.ic_broken_image_black_24dp)
                    localPhoto.setBackgroundResource(R.color.colorPrimaryDark)
                }
            } else {
                loadImages(holder.itemView, current.id)
            }
        }
    }

    internal fun setLocals(locals: List<DevByteLocal>) {
        this.locals = locals
        notifyDataSetChanged()
    }

    override fun getItemCount() = locals.size

    private fun saveImage(image: Bitmap, itemView: View, localId: String){

        val imageFileName = "JPEG_${localId}.jpg"

        // Creates a file with this name, or replaces an existing file
        // that has the same name. Note that the file name cannot contain
        // path separators.
        val encryptedFile = EncryptedFile.Builder(
                File("/data/data/com.example.cityu/images/", imageFileName),
                itemView.context,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        try {
            val asd = encryptedFile.openFileOutput()
            image.compress(Bitmap.CompressFormat.JPEG, 100, asd)
            asd.close()
        } catch (error: Exception) {
            error.printStackTrace()
        }


    }

    private fun loadImages(itemView: View, localId: String){

        val imageFileName = "JPEG_${localId}.jpg"
        val encryptedFile2 = EncryptedFile.Builder(
                File("/data/data/com.example.cityu/images/", imageFileName),
                itemView.context,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        val contents = encryptedFile2.openFileInput()
        var img: Bitmap = BitmapFactory.decodeStream(contents)

        itemView.findViewById<ImageView>(R.id.local_photo).setImageBitmap(img)
    }

}