package com.example.cityu.packages.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.database.remote.DevBytePedido

class PedidoListAdapter internal constructor(
        context: Context
) : RecyclerView.Adapter<PedidoListAdapter.PedidoViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var pedidos = emptyList<DevBytePedido>() // Cached copy of pedidos

    inner class PedidoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val numPedidoView: TextView = itemView.findViewById(R.id.num_pedido)
        val valorPedidoView: TextView = itemView.findViewById(R.id.valor_pedido)
        val horaPedidoView: TextView = itemView.findViewById(R.id.hora_pedido)
        val estadoPedidoView: TextView = itemView.findViewById(R.id.estado_pedido)
        var numPedido: Int = 1
        var complete: Int = 0
        var id: String = ""
        var localId: String = ""
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PedidoViewHolder {
        var itemView = inflater.inflate(R.layout.main_pedido_cardview, parent, false)
        val aux = PedidoViewHolder(itemView)
        itemView.setOnClickListener {
            var bundle = bundleOf("id" to aux.id, "localId" to aux.localId)
            if(aux.complete != 0)
                it.findNavController().navigate(R.id.action_fragmentPedidos_to_fragmentPedidosInfo, bundle)
            else {
                bundle = bundleOf("seePedido" to true)
                it.findNavController().navigate(R.id.action_fragmentPedidos_to_pago, bundle)
            }
        }
        return aux
    }

    override fun onBindViewHolder(holder: PedidoViewHolder, position: Int) {
        val current = pedidos[position]

        holder.id = current.id!!
        holder.numPedidoView.text = "Pedido ${position+1}"
        holder.numPedido += 1
        holder.valorPedidoView.text = "$${current.total_value.toString()}"
        holder.horaPedidoView.text = current.date

        holder.complete = current.complete!!
        var aux = current.productosPedidos!!
        if(aux.isNotEmpty())
            holder.localId = aux[0].localId

        when (current.complete!!) {
            0 -> holder.estadoPedidoView.text = "Incompleto"
            1 -> holder.estadoPedidoView.text = "En curso"
            2 -> holder.estadoPedidoView.text = "Completo"
        }

    }

    internal fun setPedidos(pedidos: List<DevBytePedido>) {
        this.pedidos = pedidos
        notifyDataSetChanged()
    }

    override fun getItemCount() = pedidos.size
}