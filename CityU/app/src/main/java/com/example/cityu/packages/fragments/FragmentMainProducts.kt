package com.example.cityu.packages.fragments


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.services.ProductViewModel
import com.example.cityu.packages.util.ProductListAdapter
import com.google.android.material.snackbar.Snackbar

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [main_promotions.newInstance] factory method to
 * create an instance of this fragment.
 */
class main_promotions : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var productViewModel: ProductViewModel
    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_main_products, container, false)

        data = bundleOf("title" to "Productos y Promociones", "frag" to "products")
        passData(data)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        val lmGrid = GridLayoutManager(view.context, 1)
        recyclerView.layoutManager = lmGrid
        val adapter = ProductListAdapter(view.context,"promo")
        recyclerView.adapter = adapter

        productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)
        productViewModel.allProducts.observe(this, Observer { promotions ->
            // Update the cached copy of the words in the adapter.
            promotions?.let { adapter.setPromotions(it) }
        })

        val cm = view.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if (!isConnected) {
            Snackbar.make(view.findViewById(R.id.coordinator), "Sin Conexión", Snackbar.LENGTH_LONG).show()
        }

        // Inflate the layout for this fragment
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }

}
