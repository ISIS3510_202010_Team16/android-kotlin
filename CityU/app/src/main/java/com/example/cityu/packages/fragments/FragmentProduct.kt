package com.example.cityu.packages.fragments


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKeys
import com.example.cityu.R
import com.example.cityu.packages.activities.ActivityNewComment
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.database.Comment.DatabaseComment
import com.example.cityu.packages.database.Comment.asDomainModel
import com.example.cityu.packages.services.CommentViewModel
import com.example.cityu.packages.util.CommentListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.coroutines.runBlocking
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BlankFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BlankFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var commentViewModel: CommentViewModel
    private lateinit var database: DatabaseReference

    private val newCommentActivityRequestCode = 1
    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
    private val masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_product, container, false)
        loadImages(view, arguments?.getString("url_image")!!)
        view.findViewById<TextView>(R.id.product_type).text = arguments?.getString("type")
        view.findViewById<TextView>(R.id.product_name).text = arguments?.getString("name")
        view.findViewById<TextView>(R.id.product_cost).text = arguments?.getInt("value").toString()
        view.findViewById<RatingBar>(R.id.ratingBarView).rating = 5.toFloat()
        view.findViewById<Button>(R.id.pedido).setOnClickListener{
            it.findNavController().navigate(R.id.action_blankFragment_to_pago, arguments)
        }

        data = bundleOf("title" to "", "frag" to "product", "url_image" to arguments?.getString("url_image"))
        passData(data)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        recyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        val adapter = CommentListAdapter(view.context)
        recyclerView.adapter = adapter

        commentViewModel = ViewModelProvider(this).get(CommentViewModel::class.java)
        commentViewModel.getCommentsByProductId(arguments?.getString("id")!!).observe(this, Observer { comments ->
            // Update the cached copy of the words in the adapter.
            comments?.let { adapter.setComments(it) }
        })

        val fab = view.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(view.context, ActivityNewComment::class.java)
            startActivityForResult(intent, newCommentActivityRequestCode)
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        database = FirebaseDatabase.getInstance().reference
        if (requestCode == newCommentActivityRequestCode && resultCode == Activity.RESULT_OK) {
            var comment = ""
            var name = ""
            var date = ""
            var starts: Float = 0.toFloat()
            var userUid = ""

            data?.getStringExtra(ActivityNewComment.INFO_COMMENT_USERUID)?.let {
                userUid = it
            }
            data?.getStringExtra(ActivityNewComment.INFO_COMMENT)?.let {
                comment = it
            }
            data?.getStringExtra(ActivityNewComment.INFO_COMMENT_NAME)?.let {
                name = it
            }
            data?.getStringExtra(ActivityNewComment.INFO_COMMENT_DATE)?.let {
                date = it
            }
            data?.getFloatExtra(ActivityNewComment.INFO_COMMENT_STARTS, 0.toFloat())?.let {
                starts = it
            }

            //commentViewModel.insert(comm)
            val idComment = database.child("Comments").push().key
            val comm = DatabaseComment(idComment!!, userUid, name, date, comment, starts, null, arguments?.getString("id")!!)
            database.child("Comments").child(idComment!!).setValue(listOf(comm).asDomainModel()[0])
        } else {
            Toast.makeText(
                    context,
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BlankFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                BlankFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    private fun loadImages(itemView: View, productId: String){

        var file = File("/data/data/com.example.cityu/images/", "JPEG_${productId}.jpg")
        if (file.exists()) {
            val imageFileName = "JPEG_${productId}.jpg"
            val encryptedFile2 = EncryptedFile.Builder(
                    File("/data/data/com.example.cityu/images/", imageFileName),
                    itemView.context,
                    masterKeyAlias,
                    EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
            ).build()

            val contents = encryptedFile2.openFileInput()
            var img: Bitmap = BitmapFactory.decodeStream(contents)

            var localPhoto: ImageView = itemView.findViewById(R.id.product_photo)
            localPhoto.setImageBitmap(img)
        }
    }
}
