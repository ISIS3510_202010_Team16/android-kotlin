package com.example.cityu.packages.database.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.cityu.packages.database.Comment.DatabaseComment
import com.example.cityu.packages.database.Comment.asDomainModel
import com.example.cityu.packages.database.remote.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PedidosRepository(private val pedidosDao: PedidosDao) {

    val allPedidos: LiveData<List<DevBytePedido>> = Transformations.map(pedidosDao.getAllPedidos()) {
        convertToDevBytePedido(it)
    }

    suspend fun insertPedido(pedido: Pedido) {
        pedidosDao.insertPedido(pedido)
    }

    suspend fun insertProductoPedido(productosPedido: ProductoPedido) {
        pedidosDao.insertProductosPedido(productosPedido)
    }

    suspend fun insertPedidoAndProductoPedido(pedido: Pedido, productosPedido: ProductoPedido) {
        pedidosDao.insertPedidoAndProductoPedido(pedido, productosPedido)
    }

    suspend fun getLocalById(localId: String): List<DevByteLocal>{
        return pedidosDao.getLocalById(localId).asDomainModel()
    }

    suspend fun getPedidoById(pedidoId: String): List<DevBytePedido>{
        return convertToDevBytePedido(pedidosDao.getPedidoById(pedidoId))
    }

    suspend fun getPedidoIncomplete(): Pedido {
        return pedidosDao.getPedidoIncomplete()
    }

    suspend fun updatePedidoAndProductosPedido(pedido:Pedido,productosPedido: ProductoPedido){
        pedidosDao.updatePedidoAndProductosPedido(pedido,productosPedido)
    }

    fun getPedidosWithProductoPedidoAndProducto(): LiveData<List<PedidoWithProductoPedido>>{
        return pedidosDao.getPedidosWithProductoPedidoAndProducto()
    }

    suspend fun getPedidoIncompleteWithProductoPedido(): List<PedidoWithProductoPedido> {
        return pedidosDao.getPedidoIncompleteWithProductoPedido()
    }

    suspend fun deletePedidoIncomplete(){
        pedidosDao.deletePedidoIncomplete()
    }

    suspend fun refreshPedidos(pedidos: List<NetworkPedido>) {
        withContext(Dispatchers.IO) {
            pedidosDao.insertAllPedidos(convertNetworkPedidoToDatabasePedido(pedidos))
            for(pedido in pedidos){
                pedidosDao.insertAllProductosPedido(convertNetworkProductoPedidoToDatabasePedidoProducto(pedido.productosPedidos!!))
            }
        }
    }
}

// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class LocalRepository(private val localDao: LocalDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allLocals: LiveData<List<DevByteLocal>> = Transformations.map(localDao.getLocals()) {
        it.asDomainModel()
    }

    suspend fun getLocalById(localId: String): List<DatabaseLocal>{
        return localDao.getLocalByLocalId(localId)
    }

    suspend fun getLocalsByCategoryId(categoryId: Int): List<DatabaseLocal>{
        return localDao.getLocalsByCategoryId(categoryId)
    }

    suspend fun insert(local: DatabaseLocal) {
        localDao.insert(local)
    }

    suspend fun updateNumStars(localId: String, stars: Float){
        localDao.updateNumStars(localId, stars)
    }

    suspend fun update(local: DatabaseLocal) {
        localDao.updateLocal(local)
    }

    suspend fun refreshLocales(locales: List<NetworkLocal>) {
        withContext(Dispatchers.IO) {
            localDao.insertAll(convertNetworkLocalToDatabaseLocal(locales))
        }
    }
}

class ProductoRepository(private val productoDao: ProductoDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allProducto: LiveData<List<DevByteProducto>> = Transformations.map(productoDao.getProducts()) {
        it.asDomainModel()
    }

    val allPromotions: LiveData<List<DevByteProducto>> = Transformations.map(productoDao.getPromotions()) {
        it.asDomainModel()
    }

    suspend fun insert(producto: DatabaseProducto) {
        productoDao.insert(producto)
    }

    suspend fun getProductById(productId: Int): List<DatabaseProducto>{
        return productoDao.getProductsById(productId)
    }

    fun getProductsByLocal(localId: String): LiveData<List<DevByteProducto>>{
        return Transformations.map(productoDao.getProductsByLocal(localId)){
            it.asDomainModel()
        }
    }

    suspend fun refreshProducto(products: List<NetworkProducto>) {
        withContext(Dispatchers.IO) {
            productoDao.insertAll(convertNetworkProductoToDatabaseProducto(products))
        }
    }


}

class CommentRepository(private val commentDao: CommentDao) {

    val allComments: LiveData<List<DevByteComment>> = Transformations.map(commentDao.getComments()) {
        it.asDomainModel()
    }

    fun getCommentsByLocal(localId: String): LiveData<List<DatabaseComment>>{
        return commentDao.getCommentsByLocalId(localId)
    }

    fun getCommentsByProductId(productId: String): LiveData<List<DatabaseComment>>{
        return commentDao.getCommentsByProductId(productId)
    }

    suspend fun insert(comment: DatabaseComment) {
        commentDao.insert(comment)
    }

    suspend fun refreshComments(comments: List<NetworkComment>) {
        withContext(Dispatchers.IO) {
            commentDao.insertAll(convertNetworkCommentToDatabaseComment(comments))
        }
    }
}