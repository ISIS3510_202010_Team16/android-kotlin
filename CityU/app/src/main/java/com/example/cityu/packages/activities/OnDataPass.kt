package com.example.cityu.packages.activities

import android.os.Bundle

interface OnDataPass {
    fun onDataPass(data: Bundle)
}
