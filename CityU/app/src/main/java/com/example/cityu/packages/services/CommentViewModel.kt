package com.example.cityu.packages.services

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.cityu.packages.database.Comment.DatabaseComment
import com.example.cityu.packages.database.local.AppRoomDatabase
import com.example.cityu.packages.database.local.CommentRepository
import com.example.cityu.packages.database.remote.DevByteComment
import com.example.cityu.packages.database.remote.NetworkComment
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException

class CommentViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CommentRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only updatePedidoAndProductosPedido the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allComments: LiveData<List<DevByteComment>>

    init {
        val commentsDao = AppRoomDatabase.getDatabase(application, viewModelScope).commentDao()
        repository = CommentRepository(commentsDao)
        refreshDataFromRepository()
        allComments = repository.allComments
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(comment: DatabaseComment) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(comment)
    }


    fun getCommentsByLocal(localId: String): LiveData<List<DatabaseComment>>{
        return repository.getCommentsByLocal(localId)
    }

    fun getCommentsByProductId(productId: String): LiveData<List<DatabaseComment>>{
        return repository.getCommentsByProductId(productId)
    }

    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    private fun refreshDataFromRepository() {
        val database = FirebaseDatabase.getInstance().reference
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to updatePedidoAndProductosPedido the UI
                val comments = dataSnapshot.children.mapNotNull { it.getValue<NetworkComment>(NetworkComment::class.java) }
                viewModelScope.launch {
                    try {
                        repository.refreshComments(comments)
                        _eventNetworkError.value = false
                        _isNetworkErrorShown.value = false
                    } catch (networkError: IOException) {
                        // Show a Toast error message and hide the progress bar.
                        if (allComments.value.isNullOrEmpty())
                            _eventNetworkError.value = true
                    }

                    // ...
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        database.child("Comments").addValueEventListener(postListener)
    }

    /**
     * Event triggered for network error. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _eventNetworkError = MutableLiveData<Boolean>(false)


    /**
     * Flag to display the error message. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _isNetworkErrorShown = MutableLiveData<Boolean>(false)
}