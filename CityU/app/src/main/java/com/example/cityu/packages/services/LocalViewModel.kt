package com.example.cityu.packages.services

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.cityu.packages.database.local.DatabaseLocal
import com.example.cityu.packages.database.local.LocalRepository
import com.example.cityu.packages.database.local.AppRoomDatabase
import com.example.cityu.packages.database.remote.DevByteLocal
import com.example.cityu.packages.database.remote.NetworkLocal
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException

class LocalViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: LocalRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only updatePedidoAndProductosPedido the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allLocals: LiveData<List<DevByteLocal>>

    init {
        val localsDao = AppRoomDatabase.getDatabase(application, viewModelScope).localDao()
        repository = LocalRepository(localsDao)
        refreshDataFromRepository()
        allLocals = repository.allLocals
    }

    suspend fun getLocalById(localId: String): List<DatabaseLocal>{
        return repository.getLocalById(localId)
    }

    suspend fun getLocalsByCategoryId(categoryId: Int): List<DatabaseLocal>{
        return repository.getLocalsByCategoryId(categoryId)
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(local: DatabaseLocal) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(local)
    }

    fun update(local: DatabaseLocal) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(local)
    }

    fun updateNumStars(localId: String, stars: Float) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateNumStars(localId, stars)
    }

     /**
      * Refresh data from the repository. Use a coroutine launch to run in a
      * background thread.
      */
     private fun refreshDataFromRepository() {
         val database = FirebaseDatabase.getInstance().reference
         val postListener = object : ValueEventListener {
             override fun onDataChange(dataSnapshot: DataSnapshot) {
                 // Get Post object and use the values to updatePedidoAndProductosPedido the UI
                 val locales = dataSnapshot.children.mapNotNull { it.getValue<NetworkLocal>(NetworkLocal::class.java) }
                 viewModelScope.launch {
                     try {
                         repository.refreshLocales(locales)
                         _eventNetworkError.value = false
                         _isNetworkErrorShown.value = false
                     } catch (networkError: IOException) {
                         // Show a Toast error message and hide the progress bar.
                         if (allLocals.value.isNullOrEmpty())
                             _eventNetworkError.value = true
                     }

                     // ...
                 }
             }

             override fun onCancelled(databaseError: DatabaseError) {
                 // Getting Post failed, log a message
                 Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                 // ...
             }
         }
         database.child("Locales").addValueEventListener(postListener)
     }

     /**
      * Event triggered for network error. This is private to avoid exposing a
      * way to set this value to observers.
      */
     private var _eventNetworkError = MutableLiveData<Boolean>(false)


     /**
      * Flag to display the error message. This is private to avoid exposing a
      * way to set this value to observers.
      */
     private var _isNetworkErrorShown = MutableLiveData<Boolean>(false)


}