package com.example.cityu.packages.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import androidx.appcompat.app.AppCompatActivity
import com.example.cityu.R
import com.example.cityu.packages.database.remote.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*

class ActivityNewComment: AppCompatActivity(), RatingBar.OnRatingBarChangeListener {

    private lateinit var editCommentView: EditText
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_comment)
        editCommentView = findViewById(R.id.edit_comment)


        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().reference
        val idUser = auth.currentUser?.uid.toString()

        val rBar = findViewById<RatingBar>(R.id.ratingBar)
        rBar.onRatingBarChangeListener = this

        val replyIntent = Intent()
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var infoUser = dataSnapshot.getValue<User>(User::class.java)
                for(i in dataSnapshot.children)
                    println(i)
                replyIntent.putExtra(INFO_COMMENT_NAME, "${infoUser?.nombre} ${infoUser?.apellido}")
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        println(idUser)
        database.child("User").child(idUser).addValueEventListener(postListener)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {
            if (TextUtils.isEmpty(editCommentView.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val sdf = SimpleDateFormat("dd/M/yyyy")
                val currentDate = sdf.format(Date())
                replyIntent.putExtra(INFO_COMMENT_USERUID, idUser)
                replyIntent.putExtra(INFO_COMMENT_STARTS, rBar.rating)
                replyIntent.putExtra(INFO_COMMENT_DATE, currentDate.toString())
                replyIntent.putExtra(INFO_COMMENT, editCommentView.text.toString())
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    override fun onRatingChanged(p0: RatingBar?, p1: Float, p2: Boolean) {
        //println(p1)
    }

    companion object {
        const val EXTRA_REPLY = "com.example.android.wordlistsql.REPLY"
        const val INFO_COMMENT = "INFO_COMMENT"
        const val INFO_COMMENT_NAME = "INFO_COMMENT_NAME"
        const val INFO_COMMENT_DATE = "INFO_COMMENT_DATE"
        const val INFO_COMMENT_STARTS = "INFO_COMMENT_STARS"
        const val INFO_COMMENT_USERUID = "INFO_COMMENT_USERSUID"
    }
}

