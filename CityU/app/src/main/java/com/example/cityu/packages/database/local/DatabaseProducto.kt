package com.example.cityu.packages.database.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.cityu.packages.database.remote.DevByteProducto

@Entity
data class DatabaseProducto(
        @PrimaryKey val id: String = "",
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "value") val value: Int,
        @ColumnInfo(name = "type") val type: String,
        @ColumnInfo(name = "type_value") val type_value: String = "",
        @ColumnInfo(name = "localId") val localId: String,
        @ColumnInfo(name = "url_product") val url_product: String = "")

/**
 * Map DatabaseVideos to domain entities
 */
fun List<DatabaseProducto>.asDomainModel(): List<DevByteProducto> {
    return map {
        DevByteProducto(
                id = it.id,
                name = it.name,
                value = it.value,
                type = it.type,
                type_value = it.type_value,
                localId = it.localId,
                url_product = it.url_product)
    }
}
