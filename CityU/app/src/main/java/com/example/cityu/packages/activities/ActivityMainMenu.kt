package com.example.cityu.packages.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.example.cityu.R
import com.google.firebase.auth.FirebaseAuth
import android.util.DisplayMetrics
import android.widget.ImageView
import androidx.navigation.NavController
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKeys
import java.io.File


class ActivityMainMenu : AppCompatActivity(), OnDataPass {


    private lateinit var op: Button

    private lateinit var top1: ImageView
    private lateinit var top2: ImageView

    private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
    private val masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)


        op = findViewById(R.id.opciones)
        top1 = findViewById(R.id.image_top1)
        top2 = findViewById(R.id.image_top2)

        op.visibility = View.VISIBLE
        op.setOnClickListener {
            op.visibility = View.INVISIBLE
            findNavController(R.id.fragmentMainMenu).navigate(R.id.action_fragment_main_menu_to_options)
        }
    }

    override fun onDataPass(data: Bundle) {
        findViewById<TextView>(R.id.mainmenu).text = data.getString("title")
        val frag = data.getString("frag")
        if(frag == "main")
            op.visibility = View.VISIBLE
        else
            op.visibility = View.INVISIBLE

        if(frag == "local" || frag == "product") {
            top1.visibility = View.INVISIBLE
            top2.visibility = View.VISIBLE

            var urlImage = data.getString("url_image")
            var file = File("/data/data/com.example.cityu/images/", "JPEG_${urlImage}.jpg")
            if (file.exists())
                loadImages(top2, urlImage!!)

        }
        else{
            top1.visibility = View.VISIBLE
            top2.visibility = View.INVISIBLE
        }
    }


    private fun loadImages(imageView: ImageView, localId: String){

        val imageFileName = "JPEG_${localId}.jpg"
        val encryptedFile2 = EncryptedFile.Builder(
                File("/data/data/com.example.cityu/images/", imageFileName),
                this,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        val contents = encryptedFile2.openFileInput()
        var img: Bitmap = BitmapFactory.decodeStream(contents)

        imageView.setImageBitmap(img)
    }

    override fun onBackPressed() {
        var controller:NavController = findNavController(R.id.fragmentMainMenu)
        if (controller.currentDestination?.id == R.id.fragment_main_menu) {
            finish()
        } else {
            super.onBackPressed()
        }
    }
}