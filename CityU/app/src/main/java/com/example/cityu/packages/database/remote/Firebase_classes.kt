package com.example.cityu.packages.database.remote
import com.example.cityu.packages.database.Comment.DatabaseComment
import com.example.cityu.packages.database.local.DatabaseLocal
import com.example.cityu.packages.database.local.DatabaseProducto
import com.example.cityu.packages.database.local.Pedido
import com.example.cityu.packages.database.local.ProductoPedido
import com.google.firebase.database.Exclude

data class NetworkLocal(
        var id: String = "",
        var name: String = "",
        var addr: String = "",
        var phone: String = "",
        var web_page: String = "",
        var wait_time: Int = 0,
        var num_stars: Float = 0.toFloat(),
        var category: Int = 1,
        var num_comments: Int = 0,
        var url_image: String = "") {

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
                "id" to id,
                "name" to name,
                "addr" to addr,
                "phone" to phone,
                "web_page" to web_page,
                "wait_time" to wait_time,
                "num_stars" to num_stars,
                "category" to category
        )
    }
}

fun convertNetworkLocalToDatabaseLocal(locales: List<NetworkLocal>): List<DatabaseLocal>{
    return locales.map {
        DatabaseLocal(
                id = it.id,
                name = it.name,
                addr = it.addr,
                phone = it.phone,
                web_page = it.web_page,
                wait_time = it.wait_time,
                num_stars = it.num_stars,
                category = it.category,
                num_comments = it.num_comments,
                url_image = it.url_image)
    }
}

/*----------------------------------------------------------------------------------------*/
data class NetworkProducto(
        var id: String = "",
        var name: String = "",
        var value: Int = 0,
        var type: String = "",
        var type_value: String = "",
        var localId: String = "",
        var url_product: String = "")

fun convertNetworkProductoToDatabaseProducto(promotions: List<NetworkProducto>): List<DatabaseProducto>{
    return promotions.map {
        DatabaseProducto(
                id = it.id,
                name = it.name,
                value = it.value,
                type = it.type,
                type_value = it.type_value,
                localId = it.localId,
                url_product = it.url_product)
    }
}

/*----------------------------------------------------------------------------------------*/
data class NetworkPedido(
        var id: String = "",
        var productosPedidos: List<NetworkProductoPedido>? = arrayListOf(),
        var total_value: Int = 0,
        var complete: Int = 0,
        var date: String = "")

data class NetworkProductoPedido(
        var pedidoId: String = "",
        var productId: String = "",
        var productName: String = "",
        var quantity: Int = 0,
        var cost: Int = 0,
        var localId: String = "")

fun convertNetworkPedidoToDatabasePedido(pedidos: List<NetworkPedido>): List<Pedido>{
    return pedidos.map {
        Pedido(
                id = it.id,
                total_value = it.total_value,
                complete = it.complete,
                date = it.date)
    }
}

fun convertNetworkProductoPedidoToDatabasePedidoProducto(productosPedido: List<NetworkProductoPedido>): List<ProductoPedido>{
    return productosPedido.map {
        ProductoPedido(
                pedidoId = it.pedidoId,
                productId = it.productId,
                quantity  = it.quantity,
                cost = it.cost,
                localId = it.localId)
    }
}

/*----------------------------------------------------------------------------------------*/
data class NetworkComment(
        var id: String = "",
        var userUid: String = "",
        var name: String = "",
        var date: String = "",
        var comment: String = "",
        var num_stars: Float = 0.toFloat(),
        var localId: String = "",
        var productId: String = "")

fun convertNetworkCommentToDatabaseComment(comments: List<NetworkComment>): List<DatabaseComment>{
    return comments.map {
        DatabaseComment(
                id = it.id,
                userUid = it.userUid,
                name = it.name,
                date = it.date,
                comment  = it.comment,
                numStars = it.num_stars,
                localId = it.localId,
                productId = it.productId
        )
    }
}

/*----------------------------------------------------------------------------------------*/
data class User(
        var nombre: String = "",
        var apellido: String = "",
        var edad: Int = 0,
        var email: String = "",
        var genero: String = ""
)
