package com.example.cityu.packages.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.viewModelScope

import com.example.cityu.R
import com.example.cityu.packages.database.remote.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.jar.Attributes

class Profile : Fragment() {

    private lateinit var txtName: TextView
    private lateinit var txtLastName: TextView
    private lateinit var txtEmail: TextView
    private lateinit var txtEdad: TextView
    private lateinit var txtGenero: TextView

    private lateinit var dataBase: FirebaseDatabase
    private lateinit var dbReference: DatabaseReference
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)

        txtName = view.findViewById(R.id.profileName)
        txtLastName = view.findViewById(R.id.profileLastName)
        txtEmail = view.findViewById(R.id.profileEmail)
        txtEdad = view.findViewById(R.id.profileAge)
        txtGenero = view.findViewById(R.id.profileGenre)

        dataBase= FirebaseDatabase.getInstance()
        dbReference = dataBase.reference.child("User")

        auth = FirebaseAuth.getInstance()
        fillFields()
        return view
    }

    private fun fillFields() {
        val user: FirebaseUser?=auth.currentUser
        if(user != null) {
            val postListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // Get Post object and use the values to updatePedidoAndProductosPedido the UI
                    val user = dataSnapshot.getValue<User>(User::class.java)
                    println(user)
                    txtName.text = user?.nombre
                    txtLastName.text = user?.apellido
                    txtEmail.text = user?.email
                    txtEdad.text = user?.edad.toString()
                    txtGenero.text = user?.genero
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // Getting Post failed, log a message
                    Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                    // ...
                }
            }
            dbReference.child(user.uid).addValueEventListener(postListener)
        }
    }
}
