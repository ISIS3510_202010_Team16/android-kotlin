package com.example.cityu.packages.database.local

import androidx.room.*
import com.example.cityu.packages.database.remote.DevBytePedido
import com.example.cityu.packages.database.remote.DevByteProductoPedido

@Entity
data class Pedido(
        @PrimaryKey val id: String,
        @ColumnInfo(name = "total_value") val total_value: Int,
        @ColumnInfo(name = "complete") val complete: Int,
        @ColumnInfo(name = "date") val date: String)

@Entity(primaryKeys=arrayOf("pedido_id","product_id"))
data class ProductoPedido(
        @ColumnInfo(name = "pedido_id") val pedidoId: String,
        @ColumnInfo(name = "product_id") val productId: String,
        @ColumnInfo(name = "quantity") val quantity: Int,
        @ColumnInfo(name = "cost") val cost: Int,
        @ColumnInfo(name = "local_id") val localId: String)


data class ProductoPedidoAndPromotion(
        @Embedded val productoPedido: ProductoPedido,
        @Relation(
                parentColumn = "product_id",
                entityColumn = "id"
        )
        val promotion: DatabaseProducto
)

data class PedidoWithProductoPedido(
        @Embedded val pedido: Pedido,
        @Relation(
                entity = ProductoPedido::class,
                parentColumn = "id",
                entityColumn = "pedido_id"
        )
        val productosPedido: List<ProductoPedidoAndPromotion>
)


/**
 * Map DatabaseVideos to domain entities
 */
fun convertToDevBytePedido(pedido: List<PedidoWithProductoPedido>): List<DevBytePedido>{
        return pedido.map {
                DevBytePedido(
                        id = it.pedido.id,
                        productosPedidos = convertToDevByteProductoPedido(it.productosPedido),
                        total_value = it.pedido.total_value,
                        complete = it.pedido.complete,
                        date = it.pedido.date)
        }
}

fun convertToDevByteProductoPedido(productoPedido: List<ProductoPedidoAndPromotion>): List<DevByteProductoPedido>{
        return productoPedido.map {
                DevByteProductoPedido(
                        pedidoId = it.productoPedido.pedidoId,
                        productId = it.promotion.id,
                        productName = it.promotion.name,
                        quantity  = it.productoPedido.quantity,
                        cost = it.productoPedido.cost,
                        localId = it.productoPedido.localId)
        }
}