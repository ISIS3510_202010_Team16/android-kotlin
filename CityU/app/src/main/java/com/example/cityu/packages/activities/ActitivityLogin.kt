package com.example.cityu.packages.activities

import android.content.Context
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKeys
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.cityu.R
import com.example.cityu.packages.database.remote.NetworkProducto
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File


class ActivityLogin : AppCompatActivity() {

    //private lateinit var database: DatabaseReference
    //private var doubleBackToExitPressedOnce = false

    private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
    private val masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_login)
        val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if (!isConnected) {
            Snackbar.make(findViewById(R.id.coordinator), "Sin Conexión", Snackbar.LENGTH_LONG).show()
        }

        val storage = FirebaseStorage.getInstance()
        val database = FirebaseDatabase.getInstance().reference
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to updatePedidoAndProductosPedido the UI
                val products = dataSnapshot.children.mapNotNull { it.getValue<NetworkProducto>(NetworkProducto::class.java) }
                GlobalScope.launch {
                    for (product in products) {
                        var file = File("/data/data/com.example.cityu/images/", "JPEG_${product.id}.jpg")
                        if (!file.exists() && product.url_product.isNotBlank()) {
                            val gsReference = storage.getReferenceFromUrl(product.url_product)

                            var file = File("/data/data/com.example.cityu/images/", "JPEG_${product.id}.jpg")
                            if (!file.exists()) {
                                Glide.with(applicationContext)
                                        .asBitmap()
                                        .load(gsReference)
                                        .into(object : SimpleTarget<Bitmap>() {
                                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                                saveImage(resource, product.id)
                                            }
                                        })
                            }
                        }
                    }
                }

            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        database.child("Products").addValueEventListener(postListener)

/*
        val storage = FirebaseStorage.getInstance()
        val gsReference = storage.getReferenceFromUrl("gs://isis3510-b7616.appspot.com/img_login.jpg")

        Glide.with(this)
                .load(gsReference)
                .into(findViewById(R.id.main_login))*/
        /*
        val comm = DatabaseComment("asdasd", "asdasd", "mateo", "asd", "probando 1 2 3", 5.toFloat(), 3)
        database.child("Comments").child("asdasd").setValue(listOf(comm).asDomainModel()[0])

        var localsId = arrayOf(1,2,3)
        var productsId = arrayOf(1,2,3)
        var productsName = arrayOf("Alitas Picantes","Papas Grandes","Limonada")
        var productsCost = arrayOf(8000,2000,1500)


        var promotion: DevByteProducto
        var idProduct: String
        for (index in productsId.indices){
            idProduct = database.child("Products").push()!!.key!!
            promotion = DevByteProducto(productsId[index],productsName[index],productsCost[index],"producto",1)
            database.child("Products").child(idProduct).setValue(promotion)
        }*/
/*
        var purchaseId = 0
        var id = 0
        var qty = 2
        var productosPedido =  listOf(DevByteProductoPedido(productsId[id],qty,productsCost[id]*qty), DevByteProductoPedido(productsId[id+1],qty+1,productsCost[id+1]*qty))
        var value = productsCost[id]*qty + productsCost[id+1]*qty
        var pedido = DevBytePedido(productosPedido, value)
        database.child("Purchase").child(purchaseId.toString()).setValue(pedido)*/

        /*  database = FirebaseDatabase.getInstance().reference
         //var id= database.child("Locales")!!.push().key!!
         database.child("Locales").child("-M5UxEQfzN4Cd7aWcl8J").setValue(DevByteLocal("-M5UxEQfzN4Cd7aWcl8J","Alitas Colombianas", "Cra 100# 94-34", "3102020578", "www.alitascolombianas.com", 10,0.toFloat(),5))
        id= database.child("Locales")!!.push().key!!
         database.child("Locales").child(id).setValue(DevByteLocal(2, "Hornitos", "Cra 100# 94-34", "3102020578", "www.hornitos.com", 25,1))
         id= database.child("Locales")!!.push().key!!
         database.child("Locales").child(id).setValue(DevByteLocal(3, "Pizza Hut", "Cra 100# 94-34", "3102020578", "www.pizzahut.com", 13,4.5.toLong()))*/

/*
        var id= database.child("Products")!!.push().key!!
        database.child("Products").child(id).setValue(DevByteProducto(6, "Mega combo",19900, "promotion",3))*/
        /*id= database.child("Products")!!.push().key!!
        database.child("Products").child(id).setValue(DevByteProducto(5, "Combo desayuno americano",3000, "50% off",2))*/

    }

    private fun saveImage(image: Bitmap, localId: String){

        val imageFileName = "JPEG_${localId}.jpg"

        // Creates a file with this name, or replaces an existing file
        // that has the same name. Note that the file name cannot contain
        // path separators.
        val encryptedFile = EncryptedFile.Builder(
                File("/data/data/com.example.cityu/images/", imageFileName),
                this,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        try {
            val asd = encryptedFile.openFileOutput()
            image.compress(Bitmap.CompressFormat.JPEG, 100, asd)
            asd.close()
        } catch (error: Exception) {
            error.printStackTrace()
        }
    }

    override fun onBackPressed() {
        var controller:NavController = findNavController(R.id.fragmentLogin)
        if(controller.currentDestination?.id == R.id.login6) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

}