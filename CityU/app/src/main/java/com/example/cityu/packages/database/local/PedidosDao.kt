package com.example.cityu.packages.database.local

import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PedidosDao {

    /*-------------------------------------GET----------------------------------------------*/
    @Query("SELECT * FROM Pedido WHERE id = 'incompleto' AND complete = 0")
    suspend fun getPedidoIncomplete(): Pedido

    @Transaction
    @Query("SELECT * FROM Pedido WHERE id = 'incompleto' AND complete = 0")
    fun getPedidosWithProductoPedidoAndProducto(): LiveData<List<PedidoWithProductoPedido>>

    @Query("SELECT * from Pedido")
    fun getAllPedidos(): LiveData<List<PedidoWithProductoPedido>>

    @Query("SELECT * from Pedido WHERE id=:pedidoId")
    suspend fun getPedidoById(pedidoId: String): List<PedidoWithProductoPedido>

    @Query("SELECT * from DatabaseLocal WHERE id = :localId")
    suspend fun getLocalById(localId: String): List<DatabaseLocal>

    @Transaction
    @Query("SELECT * FROM Pedido WHERE id = 'incompleto' AND complete = 0")
    suspend fun getPedidoIncompleteWithProductoPedido(): List<PedidoWithProductoPedido>


    /*-------------------------------------INSERT----------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllProductosPedido( productosPedido: List<ProductoPedido>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllPedidos( pedidos: List<Pedido>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPedido( pedido: Pedido)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProductosPedido( productosPedido: ProductoPedido)

    @Transaction
    suspend fun insertPedidoAndProductoPedido(pedido: Pedido, productoPedido: ProductoPedido){
        insertPedido(pedido)
        insertProductosPedido(productoPedido)
    }

    /*-------------------------------------UPDATE----------------------------------------------*/
    @Transaction
    suspend fun updatePedidoAndProductosPedido(pedido: Pedido, productosPedido: ProductoPedido){
        updatePedido(pedido)
        insertProductosPedido(productosPedido)
    }

    @Update
    suspend fun updatePedido(vararg pedido: Pedido)

    @Update
    suspend fun updateProductosPedido(vararg productosPedido: ProductoPedido)


    /*-------------------------------------DELETE----------------------------------------------*/
    @Transaction
    suspend fun deletePedidoIncomplete(){
        val pedido = getPedidoIncomplete()
        if(pedido != null) {
            deleteAllPedidoIncomplete()
            deleteAllProductosPedidoIncomplete(pedido.id)
        }
    }

    @Query("DELETE FROM Pedido WHERE complete = 0 AND id = 'incompleto'")
    suspend fun deleteAllPedidoIncomplete()

    @Query("DELETE FROM ProductoPedido WHERE pedido_id = :pedido_id")
    suspend fun deleteAllProductosPedidoIncomplete(pedido_id: String)

    @Transaction
    suspend fun deletePedidos(){
        deleteAllPedidos()
        deleteAllProductosPedido()
    }

    @Query("DELETE FROM Pedido")
    suspend fun deleteAllPedidos()

    @Query("DELETE FROM ProductoPedido")
    suspend fun deleteAllProductosPedido()

}