package com.example.cityu.packages.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.cityu.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.File;

public class Splash extends AppCompatActivity {

    private static int SPLASH_SCREEN = 2000;

    Animation top;
    ImageView splashImage;
    FirebaseAuth auth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        File directorio = new File("/data/data/com.example.cityu", "images");
        //Muestro un mensaje en el logcat si no se creo la carpeta por algun motivo
        if (!directorio.mkdirs())
            Log.e("INFO", "Error: No se creo el directorio privado");

        top = AnimationUtils.loadAnimation(this, R.anim.top_animation);

        splashImage = findViewById(R.id.splashImage);
        splashImage.setAnimation(top);
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(getUser() != null) {
                    Intent intent = new Intent(Splash.this, ActivityMainMenu.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(Splash.this, ActivityLogin.class);
                    startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    finish();
                }

            }
        }, SPLASH_SCREEN);
    }

    public FirebaseUser getUser() {
        return user;
    }
}

