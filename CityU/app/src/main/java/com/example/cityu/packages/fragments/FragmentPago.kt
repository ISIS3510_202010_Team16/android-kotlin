package com.example.cityu.packages.fragments


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.database.local.Pedido
import com.example.cityu.packages.database.local.ProductoPedido
import com.example.cityu.packages.database.local.convertToDevBytePedido
import com.example.cityu.packages.database.remote.DevBytePedido
import com.example.cityu.packages.database.remote.DevByteProductoPedido
import com.example.cityu.packages.database.remote.NetworkProducto
import com.example.cityu.packages.services.PedidosViewModel
import com.example.cityu.packages.util.ProductoPedidoListAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_pago.view.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Pago.newInstance] factory method to
 * create an instance of this fragment.
 */
class Pago : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var database: DatabaseReference

    private lateinit var auth: FirebaseAuth
    private lateinit var pedidosViewModel: PedidosViewModel

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_pago, container, false)
        pedidosViewModel = ViewModelProvider(this).get(PedidosViewModel::class.java)

        data = bundleOf("title" to "Pagos", "frag" to "pago")
        passData(data)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        recyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        val adapter = ProductoPedidoListAdapter(view.context,"pago")
        recyclerView.adapter = adapter

        if(!arguments?.getBoolean("seePedido")!!) {
            var pedidoIncomplete: Pedido
            runBlocking {
                pedidoIncomplete = pedidosViewModel.getPedidoIncomplete()
                var pedido: Pedido
                var productoPedido: ProductoPedido
                val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
                val currentDate = sdf.format(Date())
                var date = currentDate.split(" ")[1].split(":")

                if (pedidoIncomplete == null) {
                    pedido= Pedido("incompleto", arguments?.getInt("value")!!, 0, "${date[0]}:${date[1]}")
                    productoPedido = ProductoPedido(pedido.id, arguments?.getString("id")!!, 1, pedido.total_value,
                            arguments?.getString("localId")!!)
                    pedidosViewModel.insertPedidoAndProductoPedido(pedido, productoPedido)
                } else {
                    val cost = arguments?.getInt("value")!!
                    pedido = Pedido(pedidoIncomplete.id, pedidoIncomplete.total_value + cost, 0, "${date[0]}:${date[1]}")
                    productoPedido = ProductoPedido(pedidoIncomplete.id, arguments?.getString("id")!!, 1, cost,
                            arguments?.getString("localId")!!)
                    pedidosViewModel.updatePedidoAndProductosPedido(pedido, productoPedido)
                }
                view.valor_pedido.text = "$${pedido.total_value.toString()}"
            }
        }
        else{
            runBlocking {
                var pedido = pedidosViewModel.getPedidoIncomplete()
                view.valor_pedido.text = "$${pedido.total_value.toString()}"
            }
        }


        auth = FirebaseAuth.getInstance()
        var idUser = auth.currentUser?.uid.toString()
        view.pago.setOnClickListener{
            val cm = view.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

            if (isConnected) {
                database = FirebaseDatabase.getInstance().reference
                runBlocking {

                    var pedidoList = convertToDevBytePedido(pedidosViewModel.getPedidoIncompleteWithProductoPedido())
                    if (pedidoList.isNotEmpty()) {
                        var pedido = pedidoList[0]
                        var productosPedido = pedido.productosPedidos

                        var numPedidos: ArrayList<String> = arrayListOf()
                        for (producto in productosPedido!!) {
                            if ((producto.localId !in numPedidos)) {
                                numPedidos.add(producto.localId)
                            }
                        }

                        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
                        val currentDate = sdf.format(Date())
                        var date = currentDate.split(" ")[1].split(":")

                        for (id in numPedidos) {
                            var idPedido = database.child("Pedidos").child(idUser).push()!!.key!!
                            var productosGenerados: ArrayList<DevByteProductoPedido> = arrayListOf()
                            for (producto in productosPedido!!) {
                                var localId = producto.localId
                                if (localId == id) {
                                    producto.pedidoId = idPedido
                                    producto.quantity = adapter.getProductQty()
                                    productosGenerados.add(producto)
                                }
                            }
                            var totalValue = 0
                            for (product in productosGenerados) {
                                totalValue += product.cost!!
                            }
                            var local = pedidosViewModel.getlocalById(id)
                            var minutos: String = date[1]
                            var hora: String = date[0]
                            var min = minutos.toInt() + local[0].wait_time!! - 60
                            var hor = hora.toInt() + 1
                            if (minutos.toInt() + local[0].wait_time!! >= 60) {
                                if (min < 10)
                                    minutos = "0$min"
                                if (hor < 10)
                                    hora = "0$hor"
                            }
                            var pedidoGenerado = DevBytePedido(idPedido, productosGenerados, totalValue, 1, "${hora}:${minutos}")
                            database.child("Pedidos").child(idUser).child(idPedido).setValue(pedidoGenerado)
                        }
                        /*var idPedido = database.child("Pedidos").child(idUser).push()!!.key!!
                    //var idPedido = database.child("Pedidos").push()!!.key!!
                    database.child("Pedidos").child(idUser).child(idPedido).setValue(pedido)*/
                        pedidosViewModel.deletePedidoIncomplete()

                        view.findNavController().navigate(R.id.action_pago_to_fragmentPedidos, null, NavOptions.Builder()
                                .setPopUpTo(R.id.fragmentPedidos,
                                        false).build())
                    }
                }
            }
            else
                Toast.makeText(activity, "Sin Conexión. Conectese a la red y vuelva a intentarlo", Toast.LENGTH_SHORT).show()
        }


        pedidosViewModel.getPedidosWithProductoPedidoAndProducto().observe(viewLifecycleOwner, Observer {
            it?.let {
                if(it.isNotEmpty())
                    adapter.setProductoPedido(convertToDevBytePedido(it)[0].productosPedidos!!)
            }
        })

        // Inflate the layout for this fragment
        return view
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Pago.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                Pago().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }
}
