package com.example.cityu.packages.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.navigation.findNavController

import com.example.cityu.R
import com.example.cityu.packages.database.remote.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_fragment_register.*
import kotlinx.android.synthetic.main.fragment_fragment_register.view.*

class FragmentRegister : Fragment() {
    private lateinit var txtName: EditText
    private lateinit var txtLastName: EditText
    private lateinit var txtEmail: EditText
    private lateinit var txtEdad: EditText
    private lateinit var txtGenero: EditText
    private lateinit var txtPassword: EditText
    private lateinit var txtConfPassword: EditText
    private lateinit var btnregister:Button

    private lateinit var dbReference: DatabaseReference
    private lateinit var dataBase: FirebaseDatabase
    private lateinit var auth: FirebaseAuth
    private lateinit var progressBar:ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_fragment_register, container, false)
        txtName = view.findViewById(R.id.registerName)
        txtLastName = view.findViewById(R.id.registerLastName)
        txtEmail = view.findViewById(R.id.registerEmail)
        txtPassword = view.findViewById(R.id.registerPassword)
        txtConfPassword = view.findViewById(R.id.registerConfirmPassword)
        txtEdad = view.findViewById(R.id.registerAge)
        txtGenero = view.findViewById(R.id.registerGenre)
        progressBar = view.findViewById(R.id.progressBarRegister)
        btnregister = view.findViewById(R.id.registerBtn)

        dataBase= FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()

        dbReference = dataBase.reference.child("User")

        btnregister.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            btnregister.visibility = View.INVISIBLE
            createAccount()
        }
        return view
    }
    private fun createAccount(){
        val name: String = txtName.text.toString()
        val lastName: String = txtLastName.text.toString()
        val email: String = txtEmail.text.toString()
        val password: String = txtPassword.text.toString()
        val passwordConf: String = txtConfPassword.text.toString()
        val edad: Int = txtEdad.text.toString().toInt()
        val genero: String = txtGenero.text.toString()

        if(name.isNotEmpty() ||
                lastName.isNotEmpty() ||
                email.isNotEmpty() ||
                password.isNotEmpty() ||
                passwordConf.isNotEmpty() ||
                genero.isNotEmpty()){
            if(password.equals(passwordConf)){
                auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
                    task ->
                    if(task.isComplete){
                        val user:FirebaseUser?=auth.currentUser
                        user?.sendEmailVerification()
                        val userDB = dbReference.child(user?.uid.toString())
                        var profile = User(name,lastName,edad,email,genero)
                        userDB.setValue(profile)
                        view?.findNavController()?.navigate(R.id.action_register_form_to_login6)
                    } else {
                        Toast.makeText(activity, "No se pudo hacer el registo, por favor intentarlo mas tarde",Toast.LENGTH_SHORT).show()
                        progressBar.visibility = View.INVISIBLE
                        btnregister.visibility = View.VISIBLE
                    }
                }
            } else {
                Toast.makeText(activity, "Las contraseñas no coinciden",Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.INVISIBLE
                btnregister.visibility = View.VISIBLE
            }
        }else {
            Toast.makeText(activity, "Faltan campos por completar",Toast.LENGTH_SHORT).show()
            progressBar.visibility = View.INVISIBLE
            btnregister.visibility = View.VISIBLE
        }
    }
}
