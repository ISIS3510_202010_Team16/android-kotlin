package com.example.cityu.packages.fragments


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.database.local.asDomainModel
import com.example.cityu.packages.database.remote.NetworkProducto
import com.example.cityu.packages.database.remote.convertNetworkProductoToDatabaseProducto
import com.example.cityu.packages.util.ProductListAdapter
import com.google.firebase.database.*
import java.io.IOException


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentRecomendations.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentRecomendations : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var database: DatabaseReference
    private lateinit var recomendationView: LinearLayout
    private lateinit var boton1: Button

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_recomendations, container, false)

        data = bundleOf("title" to "Recomendaciones", "frag" to "rcm")
        passData(data)

        database = FirebaseDatabase.getInstance().reference

        val promotionsRecyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
        promotionsRecyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        val adapter2 = ProductListAdapter(view.context,"recom")
        promotionsRecyclerView.adapter = adapter2

        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to updatePedidoAndProductosPedido the UI
                val productos = dataSnapshot.children.mapNotNull { it.getValue<NetworkProducto>(NetworkProducto::class.java) }
                adapter2.setPromotions(convertNetworkProductoToDatabaseProducto(productos).asDomainModel())

            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        database.child("Products").orderByChild("type").equalTo("Promotion").addValueEventListener(postListener)

        return view
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentRecomendations.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentRecomendations().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
