package com.example.cityu.packages.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.database.remote.DevByteProductoPedido

class ProductoPedidoListAdapter internal constructor(
        context: Context,
        type: String
) : RecyclerView.Adapter<ProductoPedidoListAdapter.ProductoPedidoViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var productosPedido = emptyList<DevByteProductoPedido>() // Cached copy of productosPedido
    private var productQtyView: EditText? = null
    private val ye: String = type

    inner class ProductoPedidoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productNameView: TextView = itemView.findViewById(R.id.product_name)
        val producCostView: TextView = itemView.findViewById(R.id.product_cost)
        val qtyTextView: TextView = itemView.findViewById(R.id.product_qty1)
        val item: View? = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoPedidoViewHolder {

        var itemView = inflater.inflate(R.layout.producto_pedido, parent, false)
        return ProductoPedidoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductoPedidoViewHolder, position: Int) {
        val current = productosPedido[position]
        holder.productNameView.text = current.productName
        productQtyView = holder.item?.findViewById(R.id.product_qty2)

        if(ye == "pago") {
            holder.qtyTextView.visibility = View.INVISIBLE
            productQtyView?.visibility = View.VISIBLE
            productQtyView?.setText(current.quantity.toString(), TextView.BufferType.EDITABLE)
        } else {
            holder.qtyTextView.visibility = View.VISIBLE
            productQtyView?.visibility = View.INVISIBLE
            holder.qtyTextView.text = current.quantity.toString()
        }

        holder.producCostView.text = "$${current.cost.toString()}"
        productQtyView = holder.item?.findViewById(R.id.product_qty2)!!
    }

    internal fun setProductoPedido(productoPedido: List<DevByteProductoPedido>) {
        this.productosPedido = productoPedido
        notifyDataSetChanged()
    }

    override fun getItemCount() = productosPedido.size

    internal fun getProductQty(): Int{
        return productQtyView?.text.toString().toInt()
    }
}