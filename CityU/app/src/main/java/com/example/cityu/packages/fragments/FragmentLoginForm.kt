package com.example.cityu.packages.fragments

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.cityu.R
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth

class FragmentLoginForm : Fragment() {

    private lateinit var txtEmail: EditText
    private lateinit var txtPassword: EditText
    private lateinit var progressBar:ProgressBar
    private lateinit var loginBtn:Button
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_login_form, container, false)

        txtEmail = view.findViewById(R.id.loginEmail)
        txtPassword = view.findViewById(R.id.loginPassword)
        progressBar = view.findViewById(R.id.progressBarLogin)
        auth = FirebaseAuth.getInstance()
        loginBtn = view.findViewById(R.id.loginBtn)

        loginBtn.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            loginBtn.visibility = View.INVISIBLE
            loginUser(view)
        }
        return view
    }

    private fun loginUser(view: View) {
        val email: String = txtEmail.text.toString()
        val password: String = txtPassword.text.toString()

        val cm = view.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if (!isConnected) {
            Snackbar.make(view.findViewById(R.id.coordinator), "Sin Conexión. Intenta más tarde", Snackbar.LENGTH_LONG).show()
            progressBar.visibility = View.INVISIBLE
            loginBtn.visibility = View.VISIBLE
        }
        else{
            if (email.isNotEmpty() && password.isNotEmpty()) {
                auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                if(!isConnected)
                                    view.findNavController().navigate(R.id.action_login_form2_to_blankFragment2)
                                else
                                    view.findNavController().navigate(R.id.action_login_form2_to_activityMainMenu2)
                            } else {
                                Toast.makeText(activity, "Correo o contraseña incorrectos", Toast.LENGTH_SHORT).show()
                                progressBar.visibility = View.INVISIBLE
                                loginBtn.visibility = View.VISIBLE
                            }
                        }
            }
            else{
                Toast.makeText(activity, "Por favor Ingresa datos en los campos", Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.INVISIBLE
                loginBtn.visibility = View.VISIBLE
            }
        }
    }
}
