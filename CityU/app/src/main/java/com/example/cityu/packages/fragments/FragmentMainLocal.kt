package com.example.cityu.packages.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.cityu.R
import com.example.cityu.packages.activities.ActivityNewComment
import com.example.cityu.packages.activities.OnDataPass
import com.example.cityu.packages.database.Comment.DatabaseComment
import com.example.cityu.packages.database.Comment.asDomainModel
import com.example.cityu.packages.database.remote.NetworkComment
import com.example.cityu.packages.services.CommentViewModel
import com.example.cityu.packages.services.LocalViewModel
import com.example.cityu.packages.services.ProductViewModel
import com.example.cityu.packages.util.CommentListAdapter
import com.example.cityu.packages.util.ProductListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.firebase.database.*
import kotlinx.coroutines.runBlocking

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [main_local.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [main_local.newInstance] factory method to
 * create an instance of this fragment.
 */

class FragmentMainLocal: Fragment() {
    // When requested, this adapter returns a DemoObjectFragment,
    // representing an object in the collection.
    private lateinit var demoCollectionPagerAdapter: DemoCollectionPagerAdapter
    private lateinit var viewPager: ViewPager

    lateinit var dataPasser: OnDataPass
    private lateinit var data: Bundle

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_local, container, false)
        data = bundleOf("title" to "", "frag" to "local", "url_image" to arguments?.getString("url_image"))
        passData(data)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        demoCollectionPagerAdapter = DemoCollectionPagerAdapter(childFragmentManager, arguments?.getString("localId")!!)
        viewPager = view.findViewById(R.id.pager)
        viewPager.adapter = demoCollectionPagerAdapter

        val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        tabLayout.setupWithViewPager(viewPager)

        val local_name = view.findViewById<TextView>(R.id.local_name)
        val waitTime = view.findViewById<TextView>(R.id.local_wait_time)
        local_name.text = arguments?.getString("name")
        waitTime.text = arguments?.getInt("waitTime").toString()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dataPasser = context as OnDataPass
    }

    fun passData(data: Bundle){
        dataPasser.onDataPass(data)
    }
}

// Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
class DemoCollectionPagerAdapter(fm: FragmentManager, localId: String) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int  = 3
    val localId = localId
    override fun getItem(i: Int): Fragment {
        val fragment = DemoObjectFragment(localId)
        fragment.arguments = Bundle().apply {
            // Our object is just an integer :-P
            putInt(ARG_OBJECT, i + 1)
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence {
        var title = "Información"
        when (position){
            1 -> title = "Calificaciones"
            2 -> title = "Promociones"
        }
        return title
    }
}

private const val ARG_OBJECT = "object"

// Instances of this class are fragments representing a single
// object in our collection.
class DemoObjectFragment(localId: String) : Fragment() {

    private val localId = localId

    private lateinit var commentViewModel: CommentViewModel
    private lateinit var productViewModel: ProductViewModel
    private lateinit var database: DatabaseReference

    private val newCommentActivityRequestCode = 1

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        var view: View = inflater.inflate(R.layout.fragment_local_information, container, false)
        database = FirebaseDatabase.getInstance().reference
        when (arguments?.get(ARG_OBJECT)){
            //2 -> view = inflater.inflate(R.layout.fragment_local_menu, container, false)
            2 -> view = inflater.inflate(R.layout.fragment_local_score, container, false)
            3 -> view = inflater.inflate(R.layout.fragment_local_products, container, false)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if(arguments?.get(ARG_OBJECT)==1) {
            val localViewModel = ViewModelProvider(this).get(LocalViewModel::class.java)
            val addr = view.findViewById<TextView>(R.id.local_addr)
            val phone = view.findViewById<TextView>(R.id.local_phone)
            val web_site = view.findViewById<TextView>(R.id.local_web_site)

            runBlocking {
                val local = localViewModel.getLocalById(localId)
                if(local.isNotEmpty()){
                    addr.text = local[0].addr
                    phone.text = local[0].phone
                    web_site.text = local[0].web_page
                }

            }

        }
        else if(arguments?.get(ARG_OBJECT)==2) {

            val recyclerView: RecyclerView = view.findViewById(R.id.recyclerId)
            recyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
            val adapter = CommentListAdapter(view.context)
            recyclerView.adapter = adapter

            commentViewModel = ViewModelProvider(this).get(CommentViewModel::class.java)
            commentViewModel.getCommentsByLocal(localId).observe(this, Observer { comments ->
                // Update the cached copy of the words in the adapter.
                comments?.let { adapter.setComments(it) }
            })

            val fab = view.findViewById<FloatingActionButton>(R.id.fab)
            fab.setOnClickListener {
                val intent = Intent(view.context, ActivityNewComment::class.java)
                startActivityForResult(intent, newCommentActivityRequestCode)
            }

        }
        else if(arguments?.get(ARG_OBJECT)==3) {

            val promotionsRecyclerView: RecyclerView = view.findViewById(R.id.recycler_promotionsId)
            promotionsRecyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
            val adapter2 = ProductListAdapter(view.context,"local")
            promotionsRecyclerView.adapter = adapter2

            productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)
            productViewModel.getProductsByLocal(localId).observe(this, Observer { promotions ->
                // Update the cached copy of the words in the adapter.
                promotions?.let { adapter2.setPromotions(it) }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newCommentActivityRequestCode && resultCode == Activity.RESULT_OK) {
            var comment = ""
            var name = ""
            var date = ""
            var stars: Float = 0.toFloat()
            var userUid = ""

            data?.getStringExtra(ActivityNewComment.INFO_COMMENT_USERUID)?.let {
                userUid = it
            }
            data?.getStringExtra(ActivityNewComment.INFO_COMMENT)?.let {
                comment = it
            }
            data?.getStringExtra(ActivityNewComment.INFO_COMMENT_NAME)?.let {
                name = it
            }
            data?.getStringExtra(ActivityNewComment.INFO_COMMENT_DATE)?.let {
                date = it
            }
            data?.getFloatExtra(ActivityNewComment.INFO_COMMENT_STARTS, 0.toFloat())?.let {
                stars = it
            }

            //commentViewModel.insert(comm)
            val idComment = database.child("Comments").push().key
            val comm = DatabaseComment(idComment!!, userUid, name, date, comment, stars, localId,null)
            database.child("Comments").child(idComment!!).setValue(listOf(comm).asDomainModel()[0])

            val localViewModel = ViewModelProvider(this).get(LocalViewModel::class.java)
            val childEventListener = object : ChildEventListener {
                override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {

                    // A new comment has been added, add it to the displayed list
                    val comment = dataSnapshot.getValue<NetworkComment>(NetworkComment::class.java)
                    runBlocking {
                        val local = localViewModel.getLocalById(localId)
                        if(local.isNotEmpty()){
                            local[0].num_comments = local[0].num_comments + 1
                            local[0].num_stars = (stars + local[0].num_stars)/local[0].num_comments
                            database.child("Locales").child(local[0].id).setValue(local[0])
                        }
                    }
                    // ...
                }

                override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
                    /*Log.d(TAG, "onChildChanged: ${dataSnapshot.key}")

                    // A comment has changed, use the key to determine if we are displaying this
                    // comment and if so displayed the changed comment.
                    val newComment = dataSnapshot.getValue<Comment>()
                    val commentKey = dataSnapshot.key*/

                    // ...
                }

                override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                    /*Log.d(TAG, "onChildRemoved:" + dataSnapshot.key!!)

                    // A comment has changed, use the key to determine if we are displaying this
                    // comment and if so remove it.
                    val commentKey = dataSnapshot.key*/

                    // ...
                }

                override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
                    /*Log.d(TAG, "onChildMoved:" + dataSnapshot.key!!)

                    // A comment has changed position, use the key to determine if we are
                    // displaying this comment and if so move it.
                    val movedComment = dataSnapshot.getValue<Comment>()
                    val commentKey = dataSnapshot.key*/

                    // ...
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    /*Log.w(TAG, "postComments:onCancelled", databaseError.toException())
                    Toast.makeText(context, "Failed to load comments.",
                            Toast.LENGTH_SHORT).show()*/
                }
            }
            database.child("Comments").addChildEventListener(childEventListener)
        } else {
            Toast.makeText(
                    context,
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show()
        }
    }

}
