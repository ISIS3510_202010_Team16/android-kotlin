package com.example.cityu.packages.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.cityu.R
import com.example.cityu.packages.database.Comment.DatabaseComment

class CommentListAdapter internal constructor(
        context: Context
) : RecyclerView.Adapter<CommentListAdapter.CommentViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var comments = emptyList<DatabaseComment>() // Cached copy of comments

    inner class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
        val dateTextView: TextView = itemView.findViewById(R.id.dateTextView)
        val commentTextView: TextView = itemView.findViewById(R.id.commentTextView)
        val ratingBarView: RatingBar = itemView.findViewById(R.id.ratingBarView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val itemView = inflater.inflate(R.layout.local_comment, parent, false)
        return CommentViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val current = comments[position]
        holder.nameTextView.text = current.name
        holder.dateTextView.text = current.date
        holder.commentTextView.text = current.comment
        holder.ratingBarView.rating = current.numStars
    }

    internal fun setComments(comments: List<DatabaseComment>) {
        this.comments = comments
        notifyDataSetChanged()
    }

    override fun getItemCount() = comments.size
}